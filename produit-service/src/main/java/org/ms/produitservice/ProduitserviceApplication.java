package org.ms.produitservice;

import org.ms.produitservice.entities.Produit;
import org.ms.produitservice.repositories.ProduitRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

@SpringBootApplication
public class ProduitserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProduitserviceApplication.class, args);
	}
	@Bean
	CommandLineRunner start(ProduitRepository produitRepository,
							RepositoryRestConfiguration repositoryRestConfiguration)
	{

		repositoryRestConfiguration.exposeIdsFor(Produit.class);
		return args ->
		{
			//Insérer trois produits de test dans la BD
			produitRepository.save(new Produit(null, "Climatiseur", 400,9));
			produitRepository.save(new Produit(null, "TV", 800,12));
			produitRepository.save(new Produit(null, "Table", 1200,3));
			//Afficher les produits existants dans la BD
			for (Produit produit : produitRepository.findAll()) {
				System.out.println(produit.toString());
			}
		};
	}
}
