package org.ms.produitservice.controllers;

import org.ms.produitservice.entities.Produit;
import org.ms.produitservice.repositories.ProduitRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.*;
import java.util.List;

@RestController("produits")
public class ProduitController {

    private final ProduitRepository produitRepository;

    public ProduitController(ProduitRepository produitRepository) {
        this.produitRepository = produitRepository;
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Produit>> getAll() {
        return ResponseEntity.ok(produitRepository.findAll());
    }

    @GetMapping("/getById")
    public ResponseEntity<Produit> getById(@RequestHeader String id) {
        return ResponseEntity.ok(produitRepository.getById(Long.parseLong(id)));
    }
}