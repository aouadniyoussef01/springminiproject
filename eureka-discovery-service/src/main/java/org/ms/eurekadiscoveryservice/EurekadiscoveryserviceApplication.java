package org.ms.eurekadiscoveryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaServer
public class EurekadiscoveryserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekadiscoveryserviceApplication.class, args);
	}


}
