package org.ms.authentificationservice.repositories;

import org.ms.authentificationservice.entities.AppRole;
import org.ms.authentificationservice.entities.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.webmvc.RepositoryRestController;

@RepositoryRestController
public interface AppRoleRepository extends JpaRepository<AppRole, Long> {
    @RestResource(path="/byRoleName")
    AppRole findByRoleName(@Param("mc") String roleName);
}
