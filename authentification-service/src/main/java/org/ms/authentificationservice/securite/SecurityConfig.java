package org.ms.authentificationservice.securite;

import org.ms.authentificationservice.entities.AppUser;
import org.ms.authentificationservice.services.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import
        org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private UserService userService;

    public SecurityConfig(UserService userService) {
        this.userService = userService;
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/swagger-ui/**", " /v3/api-docs/**");
         }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
// utiliser les données de la BD
        auth.userDetailsService(new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
// récupérer la valeur de "username" pour récupérer un objet AppUser de la BD
                AppUser appUser = userService.getUserByName(username);
//construire une collection des rôles (permissions) selon le format de SpringSecurity
                Collection<GrantedAuthority> permissions = new ArrayList<>();
//parcourir la liste des rôles de l'utilisateur pour remplir la collection des permissions
                appUser.getRoles().forEach(r -> {
                    permissions.add(new SimpleGrantedAuthority(r.getRoleName()));
                });
//retourner un objet "User" de Spring Framework qui contient: "username", "password" et les permissions
                return new User(appUser.getUsername(), appUser.getPassword(), permissions);
            }
        });
    }

    //Définir les règles d'accès
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.headers().frameOptions().disable();
        http.authorizeRequests().antMatchers("/h2-console/**","/login").permitAll();
        http.authorizeRequests().antMatchers("/swagger-ui/**", "/v3/api-docs/**", "/swagger-ui.html","/api-docs/swagger-config","/api-docs").permitAll();

//        http.formLogin();
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(new JwtAuthenticationFilter(authenticationManagerBean()));
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}